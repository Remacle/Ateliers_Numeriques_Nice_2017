%------------------------------------------------------------------------------------------------------------------
%
%   Le but ce atelier est de fabriquer un maillage optimal
%   comprenant N triangles pour la représentation de la fonction "myFunction"
%   sur un carré unité (fichier square.geo fourni). Commencez par
%   fabriquer un maillage uniforme avec gmsh (gmsh square.geo -2)
%
%   Trois étapes sont nécessaires :
%      1) calculez l'erreur d'interpolation $e_t$ sur chaque triangle $t$
%         $$ e_t^2 = \int_{t} (f - f_{fem})^2 dx dy 
%      avec f la fonction et f_{fem} son interpolée à partir des
%      valeurs nodales de f. Sauvez les valeurs de $e_t$ dans un
%      vecteur vous pouvez visualiser e en utilisant la fonction 
%      writeElementPOSFile
%       
%      2) A partir de $e$, calculez un champ de taille qui tend à
%      fabriquer un second maillage ou l'erreur sera equidistribuée
%      A la page 38 des slides fournis, vous trouverez la formule
%      magique qui permet de caculer R_t qui est le facteur de
%      réduction de taille pour le triangle t. La taille d'un
%      triangle peut être assimilée au rayon de son cercle
%      circonscrit r_t. La taille de maille dans la zone définie
%      par l'élément t est donc $h_t = r_t / R_t$. Sauvez le
%      vecteur contenant les tailles de triangle sous la même forme
%      que celui pour l'erreur ( utilisez encore une fois
%      writeElementPOSFile pour créer le ficher size.pos)
%
%      3) Utilisez gmsh pour générer le maillage optimal. Pour
%      cela, tapez gmsh square.geo -bgm size.pos. Si tout s'est
%      bien passé, le nombre de triangles de ce maillage devrait
%      avoisiner N, le paramètre qui contrôle le nombre d'éléments
%      dans le maillage optimal. Vous pouvez ensuite recommencer et
%      évaluer l'erreur dans le maillage optimal. Cette erreur
%      devrait être plus ou moins constante sur chaque élément. On
%      peut ensuite recommencer et refaire un second maillage adapté.
%
%
%------------------------------------------------------------------------------------------------------------------

function enonce (fn,N)
  mesh = load_gmsh(fn);

  %% calcul de la fonction aux noeuds du maillage
  f = zeros(mesh.nbNod,1);
  for i=1:mesh.nbNod
      f(i) = myFunction(mesh.POS(i,1),mesh.POS(i,2));
  end

  
  %% écriture de la fonction myFunction interpolée aux noeuds
  %% du maillage
  writeNodalPOSFile ('f.pos', mesh, f);
    
  err = zeros(mesh.nbTriangles,1);
  for i=1:mesh.nbTriangles
      n1 = mesh.TRIANGLES(i,1);
      n2 = mesh.TRIANGLES(i,2);
      n3 = mesh.TRIANGLES(i,3);
      X = [mesh.POS(n1,1) mesh.POS(n2,1) mesh.POS(n3,1)];
      Y = [mesh.POS(n1,2) mesh.POS(n2,2) mesh.POS(n3,2)];
      %% calculez ici l'erreur d'interpolation dans ce triangle
      %% err(i) = ...
  end

  %% vous pouvez sauver pour visualisation dans le fichier err.pos
  %% lisible par gmsh. Cette fonction crée une vue avec données par élément
  %% writeElementPOSFile ('err.pos',mesh, err);

  %% a partir de la carte d'erreurs, créér une carte de tailles
  %% et sauvez la dans un fichier 'size.pos' en utilisant la même
  %% fonction  writeElementPOSFile
  
  %% ensuite, utilisez gmsh pour créer le maillage adaptatif
  %% la commande est gmsh square.geo -bgm size.pos -2
  
end

%% fonction qui calcule la "taille" d'un triangle i.e. le
%% rayon de son cercle circonscrit

function r = circumRadius (X,Y)
    
    a = sqrt ((X(3)-X(1))^2+(Y(3)-Y(1))^2);
    b = sqrt ((X(3)-X(2))^2+(Y(3)-Y(2))^2);
    c = sqrt ((X(2)-X(1))^2+(Y(2)-Y(1))^2);
    Jac = [X(3)-X(1) X(2)-X(1) ; Y(3)-Y(1) Y(2)-Y(1) ];
    Det = det (Jac);

    r = a*b*c/abs(Det);
    
end

%% points de gauss intégrant exactement un polynome d'ordre 4
function val = interpolation_error (X, Y, f)
    GQT4 = [0.816847572980459 0.091576213509771 0.054975871827661; ....
            0.091576213509771 0.816847572980459 0.054975871827661;...
            0.091576213509771 0.091576213509771 0.054975871827661;...
            0.108103018168070 0.445948490915965 0.111690794839005;...
            0.445948490915965 0.108103018168070 0.111690794839005;...
            0.445948490915965 0.445948490915965 0.111690794839005];
end

%% un exemple de fonction ...
function f = myFunction (x,y)
    f = atanh (6*(sqrt((x-.5)^2+(y-.5)^2)-.2));
end

function writeElementPOSFile (fn, mesh, v)
    fid = fopen(fn, 'w');
    fprintf(fid,'View \"\"{\n');    
    for i=1:mesh.nbTriangles
        n1 = mesh.TRIANGLES(i,1);
        n2 = mesh.TRIANGLES(i,2);
        n3 = mesh.TRIANGLES(i,3);
        X = [mesh.POS(n1,1) mesh.POS(n2,1) mesh.POS(n3,1)];
        Y = [mesh.POS(n1,2) mesh.POS(n2,2) mesh.POS(n3,2)];
        fprintf(fid,'ST(%g,%g,%g,%g,%g,%g,%g,%g,%g){%g,%g,%g};\n',...
                X(1),Y(1),0.0,X(2),Y(2),0.0,X(3),Y(3),0.0,v(i),v(i),v(i));
    end
    fprintf(fid,'};\n');    
    fclose(fid);    
end

function writeNodalPOSFile (fn, mesh, v)
    fid = fopen(fn, 'w');
    fprintf(fid,'View \"\"{\n');    
    for i=1:mesh.nbTriangles
        n1 = mesh.TRIANGLES(i,1);
        n2 = mesh.TRIANGLES(i,2);
        n3 = mesh.TRIANGLES(i,3);
        X = [mesh.POS(n1,1) mesh.POS(n2,1) mesh.POS(n3,1)];
        Y = [mesh.POS(n1,2) mesh.POS(n2,2) mesh.POS(n3,2)];
        fprintf(fid,'ST(%g,%g,%g,%g,%g,%g,%g,%g,%g){%g,%g,%g};\n',...
                X(1),Y(1),0.0,X(2),Y(2),0.0,X(3),Y(3),0.0,v(n1),v(n2),v(n3));
    end
    fprintf(fid,'};\n');    
    fclose(fid);    
end

