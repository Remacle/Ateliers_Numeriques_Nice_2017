function corrige (fn,N)
  mesh = load_gmsh(fn);
  err = zeros(mesh.nbTriangles,1);
  f = zeros(mesh.nbNod,1);
  for i=1:mesh.nbNod
      f(i) = myFunction(mesh.POS(i,1),mesh.POS(i,2));
  end
  for i=1:mesh.nbTriangles
      n1 = mesh.TRIANGLES(i,1);
      n2 = mesh.TRIANGLES(i,2);
      n3 = mesh.TRIANGLES(i,3);
      X = [mesh.POS(n1,1) mesh.POS(n2,1) mesh.POS(n3,1)];
      Y = [mesh.POS(n1,2) mesh.POS(n2,2) mesh.POS(n3,2)];
      err(i) =  interpolation_error (X, Y, @myFunction);
  end
  writeElementPOSFile ('err.pos',mesh, err);
  writeNodalPOSFile ('f.pos', mesh, f);
  sf = size_field (mesh, err, N);
  writeElementPOSFile ('size.pos',mesh, sf);

  x = poisson (mesh, @ONE);
  writeNodalPOSFile ('x.pos', mesh, x);
end

function sf = size_field (mesh, err, N)
    a = 2.;
    ri = err;
    d=2.;
    FACT = 0;
    for i = 1:length(err)
        FACT = FACT + err(i)^(2/(1+a));
    end    
    FACT = FACT * (a^((2+a)/(1+a)) + a^(1/(1+a)));
    for i = 1:length(err)
        e = err(i);
        ri(i)= e^(2/(2*(1+a)))*a^(1/(d*(1+a)))* ((1+a)*N/FACT)^(1/d);
    end   
    
    sf = err;
    for i=1:mesh.nbTriangles
        n1 = mesh.TRIANGLES(i,1);
        n2 = mesh.TRIANGLES(i,2);
        n3 = mesh.TRIANGLES(i,3);
        X = [mesh.POS(n1,1) mesh.POS(n2,1) mesh.POS(n3,1)];
        Y = [mesh.POS(n1,2) mesh.POS(n2,2) mesh.POS(n3,2)];
        sf(i) = circumRadius (X,Y) / ri(i)
    end    
    
end

function r = circumRadius (X,Y)
    
    a = sqrt ((X(3)-X(1))^2+(Y(3)-Y(1))^2);
    b = sqrt ((X(3)-X(2))^2+(Y(3)-Y(2))^2);
    c = sqrt ((X(2)-X(1))^2+(Y(2)-Y(1))^2);
    Jac = [X(3)-X(1) X(2)-X(1) ; Y(3)-Y(1) Y(2)-Y(1) ];
    Det = det (Jac);

    r = a*b*c/abs(Det);
    
end


function val = interpolation_error (X, Y, f)
    GQT4 = [0.816847572980459 0.091576213509771 0.054975871827661; ....
            0.091576213509771 0.816847572980459 0.054975871827661;...
            0.091576213509771 0.091576213509771 0.054975871827661;...
            0.108103018168070 0.445948490915965 0.111690794839005;...
            0.445948490915965 0.108103018168070 0.111690794839005;...
            0.445948490915965 0.445948490915965 0.111690794839005];
    val = 0;
    Jac = [X(3)-X(1) X(2)-X(1) ; Y(3)-Y(1) Y(2)-Y(1) ];
    Det = det(Jac);
    
    fnodal = [f(X(1),Y(1)) f(X(2),Y(2)) f(X(3),Y(3))];
    
    for i = 1:6
        u = GQT4(i,1);
        v = GQT4(i,2);
        w = GQT4(i,3);
        x    = X(1) * (1-u-v) + X(2)*u + X(3)*v;
        y    = Y(1) * (1-u-v) + Y(2)*u + Y(3)*v;
        fFEM = fnodal(1) * (1-u-v) + fnodal(2)*u + fnodal(3)*v;        
        val = val + (f(x,y)-fFEM)^2 * abs(Det) * w;
    end  
    val = sqrt(val);
end


function f = myFunction (x,y)
  f = atanh (6*(sqrt((x-.5)^2+(y-.5)^2)-.2));
  % f = (x*y)^2;
end

function f = ONE (x,y)
    f = 1.;
end


function writeElementPOSFile (fn, mesh, v)
    fid = fopen(fn, 'w');
    fprintf(fid,'View \"\"{\n');    
    for i=1:mesh.nbTriangles
        n1 = mesh.TRIANGLES(i,1);
        n2 = mesh.TRIANGLES(i,2);
        n3 = mesh.TRIANGLES(i,3);
        X = [mesh.POS(n1,1) mesh.POS(n2,1) mesh.POS(n3,1)];
        Y = [mesh.POS(n1,2) mesh.POS(n2,2) mesh.POS(n3,2)];
        fprintf(fid,'ST(%g,%g,%g,%g,%g,%g,%g,%g,%g){%g,%g,%g};\n',...
                X(1),Y(1),0.0,X(2),Y(2),0.0,X(3),Y(3),0.0,v(i),v(i),v(i));
    end
    fprintf(fid,'};\n');    
    fclose(fid);    
end

function writeNodalPOSFile (fn, mesh, v)
    fid = fopen(fn, 'w');
    fprintf(fid,'View \"\"{\n');    
    for i=1:mesh.nbTriangles
        n1 = mesh.TRIANGLES(i,1);
        n2 = mesh.TRIANGLES(i,2);
        n3 = mesh.TRIANGLES(i,3);
        X = [mesh.POS(n1,1) mesh.POS(n2,1) mesh.POS(n3,1)];
        Y = [mesh.POS(n1,2) mesh.POS(n2,2) mesh.POS(n3,2)];
        fprintf(fid,'ST(%g,%g,%g,%g,%g,%g,%g,%g,%g){%g,%g,%g};\n',...
                X(1),Y(1),0.0,X(2),Y(2),0.0,X(3),Y(3),0.0,v(n1),v(n2),v(n3));
    end
    fprintf(fid,'};\n');    
    fclose(fid);    
end

function x = poisson (mesh, f)
    K = sparse (mesh.nbNod,mesh.nbNod);
    mesh.nbNod
    b = zeros (mesh.nbNod,1);
    x = zeros (mesh.nbNod,1);
    df1 = [-1 -1]; 
    df2 = [ 1  0];
    df3 = [ 0  1]; 
    for i=1:mesh.nbTriangles
        n1 = mesh.TRIANGLES(i,1);
        n2 = mesh.TRIANGLES(i,2);
        n3 = mesh.TRIANGLES(i,3);
        X = [mesh.POS(n1,1) mesh.POS(n2,1) mesh.POS(n3,1)];
        Y = [mesh.POS(n1,2) mesh.POS(n2,2) mesh.POS(n3,2)];
        Jac = [X(3)-X(1) X(2)-X(1) ; Y(3)-Y(1) Y(2)-Y(1) ];
        Det = 0.5*det (Jac);
        IJac = inv(Jac);
        D1 = df1*IJac;
        D2 = df2*IJac;
        D3 = df3*IJac;
        k = Det * [dot(D1,D1) dot(D1,D2) dot(D1,D3) ; ...
                   dot(D2,D1) dot(D2,D2) dot(D2,D3) ; ...
                   dot(D3,D1) dot(D3,D2) dot(D3,D3) ];
        indices = [n1, n2 ,n3];
        K (indices , indices ) = K (indices , indices ) + k;
        val =  Det/3.0 * f((X(1)+X(2)+X(3))/3,(Y(1)+Y(2)+Y(3))/3);
        b(n1) = b(n1) + val;
        b(n2) = b(n2) + val;
        b(n3) = b(n3) + val;
    end
    
    for i=1:mesh.nbLines
        n1 = mesh.LINES(i,1);
        n2 = mesh.LINES(i,2);
        K ( : , n1 ) = 0;
        K ( n1 , : ) = 0;
        K ( n1 , n1) = 1;
        b (n1) = 0;
        K ( : , n2 ) = 0;
        K ( n2 , : ) = 0;
        K ( n2 , n2) = 1;
        b (n2) = 0;
    end

    x = K \ b;
    
end

